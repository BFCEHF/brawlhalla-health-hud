import java.awt.AWTException;
import java.awt.Color;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Robot;

public class cOLORpICKER {

	private static int[] ba = { 255, 204, 153, 102, 51, };

	private static int[] ga = { 255, 234, 214, 193, 173, 153, 122, 91, 61, 30 };

	private static int[] ra = { 255, 242, 229, 216, 203, 191, 180, 170, 160, 150, 140, 126, 113, 100, 87, 74 };

	public static int greatHealth(int r, int g, int b) {
		int health = 0;

		if (b > 0) {
			for (int i : ba) {
				if (b >= i) {
					b = i;
					break;
				}
			}
		} else if (g > 0) {
			for (int i : ga) {
				if (g >= i) {
					g = i;
					break;
				}
			}
		} else if (r > 0) {
			for (int i : ra) {
				if (r >= i) {
					r = i;
					break;
				}
			}
			if (r < 74) {
				r = 74;
			}
		}

		if (b > 0) {
			int coolbeanns = b / 51;
			health = Math.abs(coolbeanns - 5) * 10;
		} else if (g > 0) {
			switch (g) {
			case 255:
				health = 50;
				break;
			case 234:
				health = 60;
				break;
			case 214:
				health = 70;
				break;
			case 193:
				health = 80;
				break;
			case 173:
				health = 90;
				break;
			case 153:
				health = 100;
				break;
			case 122:
				health = 110;
				break;
			case 91:
				health = 120;
				break;
			case 61:
				health = 130;
				break;
			case 30:
				health = 140;
				break;
			}
		} else if (r > 0) {
			switch (r) {
			case 255:
				health = 150;
				break;
			case 242:
				health = 160;
				break;
			case 229:
				health = 170;
				break;
			case 216:
				health = 180;
				break;
			case 203:
				health = 190;
				break;
			case 191:
				health = 200;
				break;
			case 180:
				health = 210;
				break;
			case 170:
				health = 220;
				break;
			case 160:
				health = 230;
				break;
			case 150:
				health = 240;
				break;
			case 140:
				health = 250;
				break;
			case 126:
				health = 260;
				break;
			case 113:
				health = 270;
				break;
			case 100:
				health = 280;
				break;
			case 87:
				health = 290;
				break;
			case 74:
				health = 300;
				break;
			}
		} else if (health < 74) {
			health = 99999;
		}

		return health;
	}

	public static String getHealth(int n, int colorpickoffset) {
		try {
			long time = System.currentTimeMillis();
			Robot robot = new Robot();
			GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
			int width = gd.getDisplayMode().getWidth();
			int height = gd.getDisplayMode().getHeight();
			Color UL = null;
			Color UR = null;
			Color LL = null;
			Color LR = null;
			if(n == 1) {
				 UL = robot.getPixelColor(1763, 87 + colorpickoffset);
				 UR = robot.getPixelColor(1862, 87 + colorpickoffset);
				 LL = robot.getPixelColor(1763, 187 + colorpickoffset);
				 LR = robot.getPixelColor(1862, 187 + colorpickoffset);
				 String ULLH = greatHealth(UL.getRed(), UL.getGreen(), UL.getBlue()) + " DMG";
				 String URLH = greatHealth(UR.getRed(), UR.getGreen(), UR.getBlue()) + " DMG";
				 String LLLH = greatHealth(LL.getRed(), LL.getGreen(), LL.getBlue()) + " DMG";
				 String LRLH = greatHealth(LR.getRed(), LR.getGreen(), LR.getBlue()) + " DMG";
					System.out.println("Took " + (System.currentTimeMillis() - time) + "msd");
					return ULLH + "  |  " + URLH + ":" + LLLH + "  |  " + LRLH;
			} else {
				 UL = robot.getPixelColor(1763, 87 + colorpickoffset);
				 UR = robot.getPixelColor(1862, 87 + colorpickoffset);
				 String ULLH = greatHealth(UL.getRed(), UL.getGreen(), UL.getBlue()) + " DMG";
				 String URLH = greatHealth(UR.getRed(), UR.getGreen(), UR.getBlue()) + " DMG";
					System.out.println("Took " + (System.currentTimeMillis() - time) + "msd");
				 return ULLH + "  |  " + URLH;
				 
			}
			
			/*
			 * fantastic
			 */
		//	System.out.println(greatHealth(color.getRed(), color.getGreen(), color.getBlue()) + " is the health");


		} catch (AWTException e) {
			e.printStackTrace();
		}
		return "err";
	}
}