import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.UIManager;

public class grEATCODE extends JPanel {

	private JLabel clock;
	private JLabel MEME;
	public grEATCODE(int n, int colorpickoffset) {
		setLayout(new BorderLayout());
		if(n == 1)
			this.setPreferredSize(new Dimension(230, 60));
		else
			this.setPreferredSize(new Dimension(230, 30));
		clock = new JLabel();
		clock.setFont(UIManager.getFont("Label.font").deriveFont(Font.BOLD, 23f));
		MEME = new JLabel();
		MEME.setFont(UIManager.getFont("Label.font").deriveFont(Font.BOLD, 23f));
		 setSize(200, 200);
		    setLayout(new GridBagLayout());
		    GridBagConstraints constraints = new GridBagConstraints();

		    constraints.gridx = 0;
		    constraints.gridy = 0;
		    constraints.anchor = GridBagConstraints.CENTER;
		    
		    add(clock,constraints);

		    constraints.gridy = 1;
		 
		    add(MEME,constraints);


		tickTock(n, colorpickoffset);
		/*
		 * this junk basically makes the thing run every half second
		 */
		Timer timer = new Timer(500, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				tickTock(n, colorpickoffset);
			}
		});
		timer.setRepeats(true);
		timer.setCoalesce(true);
		timer.setInitialDelay(0);
		timer.start();
	}

	public void tickTock(int n, int colorpickoffset) {

		/*
		 * this thing actually parses the color of their health
		 */
		if(n == 1) {
			clock.setText(cOLORpICKER.getHealth(n, colorpickoffset).split(":")[0]);
			MEME.setText(cOLORpICKER.getHealth(n, colorpickoffset).split(":")[1]);
		} else
			clock.setText(cOLORpICKER.getHealth(n, colorpickoffset));
	}
}
