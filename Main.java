import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Main {

	public static void main(String[] args) {

		/*
		 * this junk asks which legend
		 */
		Object[] fullscreen = { "fullscreen", "windowed" };
		int n1 = JOptionPane.showOptionDialog(null, "Select display mode", "Mode Selection",
				JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.DEFAULT_OPTION, null, fullscreen, fullscreen[1]);
		if(n1 == 0) {
			try {
				String line;
				String pidInfo = "";

				Process p = Runtime.getRuntime().exec(System.getenv("windir") + "\\system32\\" + "tasklist.exe");

				BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));

				while ((line = input.readLine()) != null) {
					pidInfo += line;
				}

				input.close();

				if (pidInfo.contains("Brawlhalla.exe")) {
					Runtime.getRuntime().exec("taskkill /f /im explorer.exe");
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			
		}
		Object[] options = { "1v1", "2v2" };
		int n = JOptionPane.showOptionDialog(null, "Select Mode", "Mode Selection",
				JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.DEFAULT_OPTION, null, options, options[1]);
		new Main(n, n1);
	}

	public Main(int n, int n1) {
		JFrame frame = new JFrame("Brawlhalla health HUD");
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent event) {

				try {
					String line;
					String pidInfo = "";

					Process p = Runtime.getRuntime().exec(System.getenv("windir") + "\\system32\\" + "tasklist.exe");

					BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));

					while ((line = input.readLine()) != null) {
						pidInfo += line;
					}

					input.close();

					if (!pidInfo.contains("explorer.exe")) {
						Runtime.getRuntime().exec("explorer.exe");
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.exit(0);
			}
		});
		EventQueue.invokeLater(new Runnable() {

			@Override
			public void run() {
				/*
				 * this just makes the frame. useful stuff is inside of
				 * grEATCODE
				 */
				// JFrame frame = new JFrame("Brawlhalla health HUD");
				frame.setAlwaysOnTop(true);
				int y = 100;
				int colorpickoffset = 0;
				if(n != 0) {
					System.out.println("n != 0");
					y += 100;
				}
				if(n1 == 1) {
					System.out.println("n1 == 1");
					y += 20;
					colorpickoffset += 20;
				}
					frame.setLocation(1680, y);
				// frame.setUndecorated(true);
				frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
				
				frame.add(new grEATCODE(n, colorpickoffset));
				frame.pack();
				frame.setVisible(true);
			}
		});
	}
}
